import mongoose, { Model, Schema } from 'mongoose';
// @ts-ignore
import timestamps from 'mongoose-timestamp';

import { v4 as uuidv4 } from 'uuid';
import { ICountDown } from './types';

export const CountDownSchema = new Schema<ICountDown>(
  {
    _id: {
      type: String,
      required: true,
      default: uuidv4,
    },
    title: {
      type: String,
      trim: true,
      required: true,
    },
    description: {
      type: String,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now,
    },
    deletedAt: {
      type: Date,
      required: false,
    },
  },
  {
    collection: 'countdown',
  }
);

CountDownSchema.plugin(timestamps);

CountDownSchema.index({ createdAt: 1, updatedAt: 1 });

export const Countdown: Model<ICountDown> =
  mongoose.models['CountDown'] || mongoose.model<ICountDown>('CountDown', CountDownSchema);

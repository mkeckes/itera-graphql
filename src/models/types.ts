import { Document } from 'mongoose';

export interface TEntity extends Document<string> {
  _id: string;
}

export interface ICountDown extends TEntity {
  userId: string;
  coverId: string;
  title: string;
  description?: string;
  date: Date;
  deletedAt: Date;
}

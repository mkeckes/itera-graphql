import { gql, ApolloServer } from "apollo-server-micro";
import {NextApiRequest, NextApiResponse} from "next";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";
import {MarkTodoType, Todo} from "../../types";

const typeDefs = gql`
  type Todo {
    id: ID,
    title: String,
    marked: Boolean
  }

  type Query {
    todos: [Todo]
  }

  type Mutation {
    markTodo(id: String, marked: Boolean): Todo
  }
`;

const todos: Todo[] = [{
  id: "1",
  title: "Work less than 4 hours",
  marked: false
},{
  id: "2",
  title: "Kiss and hug someone special",
  marked: false
},{
  id: "3",
  title: "Put beer to the fridge",
  marked: false
},{
  id: "4",
  title: "Do some sport amigo",
  marked: false
}]

const resolvers = {
  Query: {
    todos: async () => todos,
  },
  Mutation: {
    markTodo: async (_: any, { id, marked }: MarkTodoType) => {
      const todo = todos.find((it) => it.id === id)
      if (todo) {
        todo.marked = marked
      }
      return todo;
    }
  }
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const startServer = apolloServer.start();

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
  await startServer;
  await apolloServer.createHandler({
    path: "/api/apollo",
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  },
};

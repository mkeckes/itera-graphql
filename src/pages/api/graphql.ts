import {NextApiRequest, NextApiResponse} from "next";

import { graphql, buildSchema } from 'graphql';

const schema = buildSchema(`
  type Query {
    hello: String
  }
`);

const rootValue = { hello: () => 'Hello world!' };

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
  const query = req.body
  if (query) {
    const resp = await graphql({ schema, source: query, rootValue })
    res.status(200).json(resp)
  } else {
    res.status(500)
  }
}

import { ApolloServer } from 'apollo-server-micro';
import dotenv from 'dotenv';
import connectDB from '../../utils/db';

dotenv.config();

import schema from '../../schema';
import {NextApiRequest, NextApiResponse} from "next";
import {ApolloServerPluginLandingPageGraphQLPlayground} from "apollo-server-core";

let server: ApolloServer | null = null;

const graphqlHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (server == null) {
    server = new ApolloServer({
      schema,
      plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
    });
    await server.start();
  }
  
  // @ts-ignore
  if (server.state.phase === 'started') {
    const handler = server.createHandler({ path: '/api/compose' });
    return handler(req, res);
  }
};

export const config = {
  api: {
    bodyParser: false,
  },
};

export default connectDB(graphqlHandler);

import {useMutation, useQuery} from "@apollo/client";
import {MARK_TODO, TODOS} from "../graphql";
import TodoPage from "../components/TodoPage";

export default function Home() {
    const { loading, data } = useQuery(TODOS, {
        fetchPolicy: 'cache-and-network',
    });
    const [markTodo] = useMutation(MARK_TODO, {
        refetchQueries: [{ query: TODOS }],
    });

    const onClick = async (id: String, marked: Boolean) => {
        await markTodo({
            variables: {
                id,
                marked,
            },
        });
    }

    return <TodoPage todos={data?.todos} loading={loading} onClick={onClick}/>
}

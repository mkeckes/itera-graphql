import client from "../../apollo-client";
import {TODOS} from "../graphql";
import {Todo} from "../types";
import TodoPage from "../components/TodoPage";

interface Props {
  todos: Array<Todo>
}

export default function Home({ todos }: Props) {
  return <TodoPage todos={todos} loading={false} onClick={() => {}}/>
}

export async function getStaticProps() {
  const { data } = await client.query({
    query: TODOS,
  });

  return {
    props: {
      todos: data.todos,
    },
  };
}

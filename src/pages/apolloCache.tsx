import {useMutation, useQuery} from "@apollo/client";
import {Todo} from "../types";
import {MARK_TODO, TODOS} from "../graphql";
import TodoPage from "../components/TodoPage";

interface Data {
    todos: Todo[]
}

export default function Home() {
    const { loading, data } = useQuery(TODOS, {
        fetchPolicy: 'cache-and-network',
    });
    const [markTodo, mutationData] = useMutation(MARK_TODO, {
        update: (cache, { data }) => {
            const cacheData = cache.readQuery<Data>({ query: TODOS });
            const todos = cacheData?.todos.map(u => u.id !== data.id ? u : data);

            cache.writeQuery({ query: TODOS, data: {todos} });
        },
    });

    const onClick = async (id: String, marked: Boolean) => {
        await markTodo({
            variables: {
                id,
                marked,
            },
        });
    }

    return <TodoPage todos={data?.todos} loading={loading} onClick={onClick}/>
}

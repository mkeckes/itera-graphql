import {gql} from "@apollo/client";

export const MARK_TODO = gql`
  mutation MarkTodo($id: String!, $marked: Boolean!) {
    markTodo(id: $id, marked: $marked) {
      id
      title
      marked
    }
  }
`;
export const TODOS = gql`
  query {
    todos {
      id
      title
      marked
    }
  }
`;

import mongoose from 'mongoose';
import dotenv from 'dotenv';
import {NextApiRequest, NextApiResponse} from "next";

dotenv.config();

mongoose.Promise = global.Promise;

let conn: any = null;

const initDBConnection = async () => {
  // @ts-ignore
  conn = mongoose.connect(process.env.MONGODB_URI, {
    autoIndex: true,
    keepAlive: true,
    keepAliveInitialDelay: 120,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  mongoose.connection.on('connected', () => {
    console.info(`Mongoose default connection is open to  ${process.env.MONGODB_URI}`);
  });

  mongoose.connection.on('error', err => {
    console.error(`Mongoose default connection failed: ${err}`);
  });

  mongoose.connection.on('disconnected', () => {
    console.info('Mongoose default connection is disconnected');
  });

  await conn;

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.info('Mongoose default connection disconnected due to application termination');
      process.exit(0);
    });
  });
};

const connectDB = (handler: (req: NextApiRequest, res: NextApiResponse) => any) => async (req: NextApiRequest, res: NextApiResponse) => {
  if (conn == null) {
    await initDBConnection();
  }
  return handler(req, res);
};

export default connectDB;


export interface Todo {
    id: string
    title: string
    marked: boolean
}

export interface MarkTodoType {
  id: string;
  marked: boolean;
}

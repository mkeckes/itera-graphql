import { Countdown } from '../models/countdown';
import { composeWithMongoose } from 'graphql-compose-mongoose';
import { ObjectTypeComposer, SchemaComposer } from 'graphql-compose';
import { ICountDown } from '../models/types';

export const TYPE = 'CountDownTC';

const createCountDownTC = (schemaComposer: SchemaComposer): ObjectTypeComposer<ICountDown> => {
  const CountDownTC = composeWithMongoose<ICountDown>(Countdown, { schemaComposer, name: TYPE });
  
  CountDownTC.addResolver({
    name: 'create',
    kind: 'mutation',
    type: CountDownTC.getResolver('createOne').getType(),
    args: { _id: 'String', ...CountDownTC.getResolver('createOne').getArgs() },
    resolve: async ({ args }) => {
      const countdown = await Countdown.create({
        _id: args._id,
        ...args.record,
      });

      return {
        record: countdown,
        recordId: countdown._id,
      };
    },
  });

  return CountDownTC;
};

export const composeCountDown = (schemaComposer: SchemaComposer) => {
  schemaComposer.delete(TYPE);
  const CountDownTC = createCountDownTC(schemaComposer);

  if (!schemaComposer.Query.hasField('countDownById')) {
    schemaComposer.Query.addFields({
      countDownById: CountDownTC.getResolver('findById'),
      countDownMany: CountDownTC.getResolver('findMany'),
    });
  }

  if (!schemaComposer.Query.hasField('countDownCreate')) {
    schemaComposer.Mutation.addFields({
      countDownCreate: CountDownTC.getResolver('create'),
      countDownUpdate: CountDownTC.getResolver('updateById'),
      countDownRemove: CountDownTC.getResolver('removeOne'),
      countDownRemoveMany: CountDownTC.getResolver('removeMany'),
    });
  }
};

export default composeCountDown;

import { SchemaComposer } from 'graphql-compose';
import composeCountDown from './countdown';

const schemaComposer = new SchemaComposer();

composeCountDown(schemaComposer);

export default schemaComposer.buildSchema();
